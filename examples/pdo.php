<?php

require_once __DIR__ . '/../vendor/autoload.php';

use mef\Db\Driver\PdoDriver;
use mef\Db\TransactionDriver\NestedTransactionDriver;

$pdo = new PDO('sqlite::memory:');

$driver = new PdoDriver($pdo);
$transactionEngine = new NestedTransactionDriver($driver);
$driver->setTransactionDriver($transactionEngine);

$driver->execute('CREATE TABLE test ("id" INTEGER PRIMARY KEY, "key" TEXT, "value" TEXT)');

$data = [
	[1, 'a', 'apple'],
	[2, 'b', 'brussels sprout'],
	[3, 'c', 'carrot'],
	[4, 'd', 'diced onion']
];

$st = $driver->prepare('INSERT INTO test VALUES (?,?,?)');

foreach ($data as $row)
{
	$st->setParameters($row);
	$st->execute();
}

/**
 *  SINGLE ROWS **************************************************************
 *
 *  Return by value:
 *    One value from single row - fetchValue()
 *    Associative array         - fetchRow()
 *    Indexed array             - fetchRowAsArray()
 *
 *  Modify by reference:
 *
 *    Associative array         - fetchRowInto()
 *    Indexed array             - fetchRowIntoArray()
 *    Object                    - fetchRowIntoObject()
 *
 *  MULTIPLE ROWS ************************************************************
 *
 *  TRAVERSE TYPE    KEY             ROW TYPE
 *  ---------------+---------------+-------------+----------------------------
 *  array          | indexed       | associative | fetchAll()
 *  array          | indexed       | index       | fetchAllAsArray()
 *  array          | indexed       | callback    | fetchAllWithCallback()
 *  array          | indexed       | scalar      | fetchColumn()
 *  array          | associative   | associative | fetchKeyed()
 *  array          | associative   | index       | fetchKeyedAsArray()
 *  array          | associative   | callback    | fetchKeyedWithCallback()
 *  iterator       | indexed       | associative | getIterator()
 *  iterator       | indexed       | index       | getArrayIterator()
 *  iterator       | indexed       | callback    | getCallbackIterator()
 *  iterator       | indexed       | scalar      | getColumnIterator()
 *  iterator       | associative   | associative | getKeyedIterator()
 *  iterator       | associative   | index       | getKeyedArrayIterator()
 *  iterator       | associative   | callback    | getKeyedCallbackIterator()
 *  ---------------+---------------+-------------+----------------------------
 */

$rowToObject = function($row)
{
	return (object) $row;
};


$keyedRowToObject = function($row)
{
	return [$row['key'], (object) $row];
};


// Driver queries
//

echo "--- fetchAll ---", PHP_EOL;
print_r($driver->query('SELECT * FROM test')->fetchAll());

echo "--- fetchAllAsArray ---", PHP_EOL;
print_r($driver->query('SELECT * FROM test')->fetchAllAsArray());

echo "--- fetchAllWithCallback ---", PHP_EOL;
print_r($driver->query('SELECT * FROM test')->fetchAllWithCallback($rowToObject));

echo "--- fetchColumn ---", PHP_EOL;
print_r($driver->query('SELECT * FROM test')->fetchColumn());

echo "--- fetchKeyed ---", PHP_EOL;
print_r($driver->query('SELECT * FROM test')->fetchKeyed('key'));

echo "--- fetchKeyedAsArray ---", PHP_EOL;
print_r($driver->query('SELECT * FROM test')->fetchKeyedAsArray(1));

echo "--- fetchKeyedWithCallback ---", PHP_EOL;
print_r($driver->query('SELECT * FROM test')->fetchKeyedWithCallback($keyedRowToObject));

// Prepared Statements
//
echo '--- prepare/query/fetchOne() ---', PHP_EOL;
echo $driver->prepare('SELECT "value" FROM test WHERE "key"=?', ['a'])->query()->fetchValue(), PHP_EOL;
echo $driver->prepare('SELECT "value" FROM test WHERE "key"=:key', [':key' => 'a'])->query()->fetchValue(), PHP_EOL;

// Recordsets
//
$sql = 'SELECT * FROM "test" ORDER BY "key"';

foreach ($driver->query($sql) as $row)
{
	print_r($row);
}

echo '--- query/fetchRow() ---', PHP_EOL;
$resultset = $driver->query($sql);
while ($row = $resultset->fetchRow())
{
	print_r($row);
}

echo '--- query/fetchRowAsArray() ---', PHP_EOL;
$resultset = $driver->query($sql);
while ($row = $resultset->fetchRowAsArray())
{
	print_r($row);
}

echo '--- query/fetchValue() ---', PHP_EOL;
echo $driver->query($sql)->fetchValue(), PHP_EOL;

echo '--- query/fetchRowInto() ---', PHP_EOL;
$resultset = $driver->query($sql);
$row = [];
while ($resultset->fetchRowInto($row))
{
	print_r($row);
}

echo '--- query/fetchRowIntoArray() ---', PHP_EOL;
$resultset = $driver->query($sql);
$row = [];
while ($resultset->fetchRowIntoArray($row))
{
	print_r($row);
}

echo '--- query/fetchRowIntoObject() ---', PHP_EOL;
$resultset = $driver->query($sql);
$row = new stdClass;
while ($resultset->fetchRowIntoObject($row))
{
	print_r($row);
}