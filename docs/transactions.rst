============
Transactions
============

Database drivers include the following methods to support transactions:

* ``startTransaction()``
* ``commit()``
* ``rollBack()``

It is permissible to start multiple transactions. The ``commit()`` and
``rollBack()`` pertain to the most recently started transaction. The
``mef\Db\AbstractDriver`` (which all included drivers extend) outsources the
implementation details to a ``TransactionDriverInterface`` object. If a
particular implementation cannot support transactions, then some exception
will be thrown. Note that these database drivers (extending ``AbstractDriver``)
do not have a transaction driver associated with it by default.

*NestedTransactionDriver*

The recommended driver to use is ``NestedTransactionDriver``. It supports true
nested transactions; e.g., you can roll back an inner transaction but still
commit the outer one. This requires underlying support for transactions and
save points.

.. code-block:: php

    $driver = new mef\Db\Driver\PdoDriver(new PDO('sqlite::memory:'));
    $driver->setTransactionDriver(new mef\Db\TransactionDriver\NestedTransactionDriver($driver));

    $driver->startTransaction();

    // these changes are saved

    $driver->startTransaction();
    // these changes are lost
    $driver->rollBack();

    // these changes are saved

    $driver->commit();

*EmulatedNestedTransactionDriver*

If the database supports transactions, but does not support save points, then
the ``EmulatedNestedTransactionDriver`` is the best choice. Nested transactions
are supported as long as every transaction is committed.

However, if any transaction is rolled back, then the entire transaction is
rolled back. This happens when the outermost (first) transaction is closed. If
it is attempted to be committed (despite an inner transaction failing), then an
exception is thrown and it is rolled back.

*PDOEmulatedNestedTransactionDriver*

The ``PdoEmulatedNestedTransactionDriver`` extends
``EmulatedNestedTransactionDriver`` to use PDO's transaction methods (instead of
assuming the SQL syntax). It can only be used with ``PdoDriver``.