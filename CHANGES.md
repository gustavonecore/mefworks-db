mef\Db - CHANGELOG
==================

**Version 1.1.0**, 5-Aug-2015

* Add mef\Db\Driver\AbstractDecoratorDriver

**Version 1.0.1**, 13-Nov-2014

* Fix case sensitivity in PdoStatement

**Version 1.0.0**, 5-Nov-2014

* First release
