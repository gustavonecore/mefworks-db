mef\Db - Database Driver
========================

[![Total Downloads](https://poser.pugx.org/mefworks/db/downloads.png)](https://packagist.org/packages/mefworks/db)
[![Latest Stable Version](https://poser.pugx.org/mefworks/db/v/stable.png)](https://packagist.org/packages/mefworks/db)
[![Documentation](https://readthedocs.org/projects/mefdb/badge/)](https://mefdb.readthedocs.org/en/latest/)

mef\Db is a database driver with a simple and consistent object oriented
interface that works as a decorator over PDO and mysqli extensions.
