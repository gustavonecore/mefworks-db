<?php
use mef\Db\Driver\DriverInterface;
use mef\Db\TransactionDriver\EmulatedNestedTransactionDriver;

/**
 * @coversDefaultClass mef\Db\TransactionDriver\EmulatedNestedTransactionDriver
 */
class EmulatedNestedTransactionDriverTest extends PHPUnit_Framework_TestCase
{
	/**
	 * The db BEGIN statement should only be called one time because the
	 * implementation is virtual ... only the outermost begin/commit/rollback
	 * issue commands.
	 *
	 * @covers ::start
	 * @covers ::doStart
	 */
	public function testStart()
	{
		$db = $this->getMock(DriverInterface::class);

		$db->expects($this->once())->method('execute')->with('BEGIN');

		$transaction = new EmulatedNestedTransactionDriver($db);

		$transaction->start();
		$transaction->start();
		$transaction->start();
	}

	/**
	 * @covers ::commit
	 * @covers ::doCommit
	 */
	public function testCommit()
	{
		$db = $this->getMock(DriverInterface::class);

		$transaction = new EmulatedNestedTransactionDriver($db);

		$transaction->start();
		$transaction->start();
		$transaction->start();

		$db->expects($this->once())->method('execute')->with('COMMIT');

		$transaction->commit();
		$transaction->commit();
		$transaction->commit();
	}

	/**
	 * @covers ::commit
	 * @covers ::doCommit
	 */
	public function testNotReadyToCommit()
	{
		$db = $this->getMock(DriverInterface::class);

		$transaction = new EmulatedNestedTransactionDriver($db);

		$transaction->start();
		$transaction->start();

		$db->expects($this->never())->method('execute')->with('COMMIT');

		$transaction->commit();
	}

	/**
	 * @covers ::commit
	 *
	 * @expectedException mef\Db\TransactionDriver\Exception\TransactionNotStartedException
	 */
	public function testCommitWithoutTransaction()
	{
		$db = $this->getMock(DriverInterface::class);

		$transaction = new EmulatedNestedTransactionDriver($db);
		$transaction->commit();
	}

	/**
	 * @covers ::commit
	 *
	 * @expectedException mef\Db\TransactionDriver\Exception\CommitException
	 */
	public function testCommitWithRolledBackInnerTransaction()
	{
		$db = $this->getMock(DriverInterface::class);

		$transaction = new EmulatedNestedTransactionDriver($db);
		$transaction->start();
		$transaction->start();
		$transaction->rollBack();
		$transaction->commit();
	}

	/**
	 * @covers ::rollBack
	 * @covers ::doRollBack
	 */
	public function testRollBack()
	{
		$db = $this->getMock(DriverInterface::class);

		$transaction = new EmulatedNestedTransactionDriver($db);

		$transaction->start();
		$transaction->start();
		$transaction->start();

		$db->expects($this->once())->method('execute')->with('ROLLBACK');

		$transaction->rollBack();
		$transaction->rollBack();
		$transaction->rollBack();
	}

	/**
	 * @covers ::rollBack
	 *
	 * @expectedException mef\Db\TransactionDriver\Exception\TransactionNotStartedException
	 */
	public function testRollBackWithoutTransaction()
	{
		$db = $this->getMock(DriverInterface::class);

		$transaction = new EmulatedNestedTransactionDriver($db);
		$transaction->rollBack();
	}

	/**
	 * @covers ::rollBack
	 */
	public function testRollBackFollowedByGoodTransaction()
	{
		$db = $this->getMock(DriverInterface::class);

		$transaction = new EmulatedNestedTransactionDriver($db);

		$transaction->start();
		$transaction->start();
		$transaction->rollBack();
		$transaction->rollBack();

		// The internal rollback pointer should be reset now, so the following
		// transaction succeeds.

		$transaction->start();

		$db->expects($this->once())->method('execute')->with('COMMIT');

		$transaction->commit();
	}
}

