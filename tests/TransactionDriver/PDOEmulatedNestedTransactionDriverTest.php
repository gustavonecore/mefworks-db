<?php
use mef\Db\Driver\DriverInterface;
use mef\Db\Driver\PdoDriver;
use mef\Db\TransactionDriver\PdoEmulatedNestedTransactionDriver;

/**
 * @coversDefaultClass mef\Db\TransactionDriver\PdoEmulatedNestedTransactionDriver
 */
class PDOEmulatedNestedTransactionDriverTest extends PHPUnit_Framework_TestCase
{
	/**
	 * @covers ::setDatabaseDriver
	 *
	 * @expectedException InvalidArgumentException
	 */
	public function testInvalidDriver()
	{
		$db = $this->getMock(DriverInterface::class);

		$transaction = new PdoEmulatedNestedTransactionDriver($db);
	}

	/**
	 * @covers ::setDatabaseDriver
	 */
	public function testValidDriver()
	{
		$db = $this->getMockBuilder(PdoDriver::class)->disableOriginalConstructor()->getMock();

		$transaction = new PdoEmulatedNestedTransactionDriver($db);
	}

	/**
	 * @covers ::doStart
	 */
	public function testStart()
	{
		$pdo = $this->getMockBuilder('PDO')->disableOriginalConstructor()->getMock();
		$pdo->method('getAttribute')->will($this->returnValue(PDO::ERRMODE_EXCEPTION));

		$db = $this->getMockBuilder(PdoDriver::class)->setConstructorArgs([$pdo])->getMock();
		$pdo->expects($this->once())->method('beginTransaction');

		$transaction = new PdoEmulatedNestedTransactionDriver($db);
		$transaction->start();
	}

	/**
	 * @covers ::doCommit
	 */
	public function testCommit()
	{
		$pdo = $this->getMockBuilder('PDO')->disableOriginalConstructor()->getMock();
		$pdo->method('getAttribute')->will($this->returnValue(PDO::ERRMODE_EXCEPTION));

		$db = $this->getMockBuilder(PdoDriver::class)->setConstructorArgs([$pdo])->getMock();
		$pdo->expects($this->once())->method('commit');

		$transaction = new PdoEmulatedNestedTransactionDriver($db);
		$transaction->start();
		$transaction->commit();
	}

	/**
	 * @covers ::doRollBack
	 */
	public function testRollBack()
	{
		$pdo = $this->getMockBuilder('PDO')->disableOriginalConstructor()->getMock();
		$pdo->method('getAttribute')->will($this->returnValue(PDO::ERRMODE_EXCEPTION));

		$db = $this->getMockBuilder(PdoDriver::class)->setConstructorArgs([$pdo])->getMock();
		$pdo->expects($this->once())->method('rollBack');

		$transaction = new PdoEmulatedNestedTransactionDriver($db);
		$transaction->start();
		$transaction->rollBack();
	}
}

