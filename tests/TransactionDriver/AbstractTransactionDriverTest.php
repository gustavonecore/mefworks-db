<?php
use mef\Db\Driver\DriverInterface;
use mef\Db\TransactionDriver\AbstractTransactionDriver;

/**
 * @coversDefaultClass mef\Db\TransactionDriver\AbstractTransactionDriver
 */
class AbstractTransactionDriverTest extends PHPUnit_Framework_TestCase
{
	/**
	 * @covers ::__construct
	 * @covers ::getDatabaseDriver
	 * @covers ::setDatabaseDriver
	 */
	public function testAccessors()
	{
		$db = $this->getMock(DriverInterface::class);

		$transaction = $this->getMockBuilder(AbstractTransactionDriver::class)->
			setMethods(['start', 'commit', 'rollBack'])->
			setConstructorArgs([$db])->
			getMock();

		$this->assertSame($db, $transaction->getDatabaseDriver());

		$db = $this->getMock(DriverInterface::class);
		$this->assertNotSame($db, $transaction->getDatabaseDriver());

		$transaction->setDatabaseDriver($db);
		$this->assertSame($db, $transaction->getDatabaseDriver());
	}
}

