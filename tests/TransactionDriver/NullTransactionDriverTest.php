<?php

use mef\Db\TransactionDriver\NullTransactionDriver;

/**
 * @coversDefaultClass mef\Db\TransactionDriver\NullTransactionDriver
 */
class NullTransactionDriverTest extends PHPUnit_Framework_TestCase
{
	public function setUp()
	{
		$this->transactionDriver = new NullTransactionDriver;
	}

	/**
	 * @covers ::start
	 */
	public function testStart()
	{
		$this->transactionDriver->start();
		$this->assertSame(1, $this->transactionDriver->getDepth());
	}

	/**
	 * @covers ::commit
	 */
	public function testCommit()
	{
		$this->transactionDriver->start();
		$this->transactionDriver->commit();
		$this->assertSame(0, $this->transactionDriver->getDepth());
	}

	/**
	 * @covers ::commit
	 *
	 * @expectedException mef\Db\TransactionDriver\Exception\TransactionNotStartedException
	 */
	public function testCommitWithNoTransaction()
	{
		$this->transactionDriver->commit();
	}

	/**
	 * @covers ::rollBack
	 */
	public function testRollBack()
	{
		$this->transactionDriver->start();
		$this->transactionDriver->rollBack();
		$this->assertSame(0, $this->transactionDriver->getDepth());
	}

	/**
	 * @covers ::rollBack
	 *
	 * @expectedException mef\Db\TransactionDriver\Exception\TransactionNotStartedException
	 */
	public function testRollBackWithNoTransaction()
	{
		$this->transactionDriver->rollBack();
	}

	/**
	 * @covers ::getDepth
	 */
	public function testGetDepth()
	{
		$this->assertSame(0, $this->transactionDriver->getDepth());

		$this->transactionDriver->start();
		$this->assertSame(1, $this->transactionDriver->getDepth());

		$this->transactionDriver->start();
		$this->assertSame(2, $this->transactionDriver->getDepth());

		$this->transactionDriver->rollBack();
		$this->assertSame(1, $this->transactionDriver->getDepth());

		$this->transactionDriver->commit();
		$this->assertSame(0, $this->transactionDriver->getDepth());
	}
}