<?php

use mef\Db\Driver\DriverInterface;
use mef\Db\TransactionDriver\NestedTransactionDriver;

/**
 * @coversDefaultClass mef\Db\TransactionDriver\NestedTransactionDriver
 */
class NestedTransactionDriverTest extends PHPUnit_Framework_TestCase
{
	public function setUp()
	{
		$this->db = $this->getMock(DriverInterface::class);
		$this->transaction = new NestedTransactionDriver($this->db);
	}

	/**
	 * @covers ::start
	 * @covers ::getStartSyntax
	 */
	public function testStart()
	{
		$this->db->expects($this->once())->method('execute')->with('BEGIN');

		$this->transaction->start();
	}

	/**
	 * @covers ::start
	 * @covers ::getSavePointSyntax
	 */
	public function testSavePoint()
	{
		$this->transaction->start();

		$this->db->expects($this->once())->method('execute')->with('SAVEPOINT _sp_1');
		$this->transaction->start();
	}

	/**
	 * @covers ::commit
	 * @covers ::getCommitSyntax
	 * @covers ::getReleaseSavePointSyntax
	 */
	public function testCommit()
	{
		$this->transaction->start();
		$this->transaction->start();

		$this->db->expects($this->at(0))->method('execute')->with('RELEASE SAVEPOINT _sp_1');
		$this->db->expects($this->at(1))->method('execute')->with('COMMIT');
		$this->db->expects($this->exactly(2))->method('execute');

		$this->transaction->commit();
		$this->transaction->commit();
	}

	/**
	 * @covers ::commit
	 *
	 * @expectedException mef\Db\TransactionDriver\Exception\TransactionNotStartedException
	 */
	public function testCommitWithoutTransaction()
	{
		$this->transaction->commit();
	}

	/**
	 * @covers ::rollBack
	 * @covers ::getRollBackSyntax
	 * @covers ::getRollBackSavePointSyntax
	 */
	public function testRollBack()
	{
		$this->transaction->start();
		$this->transaction->start();

		$this->db->expects($this->at(0))->method('execute')->with('ROLLBACK TO SAVEPOINT _sp_1');
		$this->db->expects($this->at(1))->method('execute')->with('ROLLBACK');
		$this->db->expects($this->exactly(2))->method('execute');

		$this->transaction->rollBack();
		$this->transaction->rollBack();
	}

	/**
	 * @covers ::rollBack
	 *
	 * @expectedException mef\Db\TransactionDriver\Exception\TransactionNotStartedException
	 */
	public function testRollBackWithoutTransaction()
	{
		$this->transaction->rollBack();
	}

	/**
	 * @covers ::getDepth
	 */
	public function testGetDepth()
	{
		$this->assertSame(0, $this->transaction->getDepth());

		$this->transaction->start();
		$this->assertSame(1, $this->transaction->getDepth());

		$this->transaction->start();
		$this->assertSame(2, $this->transaction->getDepth());

		$this->transaction->start();
		$this->assertSame(3, $this->transaction->getDepth());

		$this->transaction->rollBack();
		$this->assertSame(2, $this->transaction->getDepth());

		$this->transaction->rollBack();
		$this->assertSame(1, $this->transaction->getDepth());

		$this->transaction->commit();
		$this->assertSame(0, $this->transaction->getDepth());
	}
}