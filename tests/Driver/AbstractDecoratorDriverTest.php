<?php

use mef\Db\Driver\DriverInterface;
use mef\Db\Driver\AbstractDecoratorDriver;

/**
 * @coversDefaultClass \mef\Db\Driver\AbstractDecoratorDriver
 */
class AbstractDecoratorDriverTest extends PHPUnit_Framework_TestCase
{
	/**
	 * @covers ::__construct
	 * @covers ::startTransaction
	 * @covers ::commit
	 * @covers ::rollBack
	 * @covers ::prepare
	 * @covers ::query
	 * @covers ::execute
	 * @covers ::quoteValue
	 */
	public function testDecorations()
	{
		$this->db = $this->getMock(DriverInterface::class);

		$this->db->expects($this->once())->method('startTransaction');
		$this->db->expects($this->once())->method('commit');
		$this->db->expects($this->once())->method('rollBack');
		$this->db->expects($this->once())->method('prepare');
		$this->db->expects($this->once())->method('query');
		$this->db->expects($this->once())->method('execute');
		$this->db->expects($this->once())->method('quoteValue');

		$db = $this->getMockForAbstractClass(AbstractDecoratorDriver::class, [$this->db]);

		$db->startTransaction();
		$db->commit();
		$db->rollBack();
		$db->prepare('x', [42]);
		$db->query('x');
		$db->execute('x');
		$db->quotevalue('x');
	}
}