<?php

use mef\Db\Driver\DataProvider\SequentialArrayDataProvider;
use mef\Db\Driver\DataProvider\DataProviderInterface;

/**
 * @coversDefaultClass mef\Db\Driver\DataProvider\SequentialArrayDataProvider
 */
class SequentialArrayDataProviderTest extends PHPUnit_Framework_TestCase
{
	/**
	 * @covers ::__construct
	 */
	public function testConstructor()
	{
		$this->assertTrue(new SequentialArrayDataProvider([]) instanceof DataProviderInterface);
	}

	/**
	 * @covers ::getDataForQuery
	 */
	public function testGetDataForQuery()
	{
		$dataset = [
			1 => [['id' => 1, 'name' => 'Matthew']],
		];
		$dataProvider = new SequentialArrayDataProvider($dataset);

		// empty because index 0 does not exist
		$this->assertSame([], iterator_to_array($dataProvider->getDataForQuery('')));

		// second query corresponds with entry 1
		$this->assertSame($dataset[1], iterator_to_array($dataProvider->getDataForQuery('')));

		// all remaining queries are same as entry 1 since there are no more
		$this->assertSame($dataset[1], iterator_to_array($dataProvider->getDataForQuery('')));
	}

	/**
	 * @covers ::executeQuery
	 */
	public function testExecuteQuery()
	{
		$dataProvider = new SequentialArrayDataProvider([]);
		$this->assertSame(0, $dataProvider->executeQuery(''));
	}

	/**
	 * @covers ::getDataForStatement
	 */
	public function testGetDataForStatement()
	{
		$dataset = [
			1 => [['id' => 1, 'name' => 'Matthew']],
		];
		$dataProvider = new SequentialArrayDataProvider($dataset);

		// empty because index 0 does not exist
		$this->assertSame([], iterator_to_array($dataProvider->getDataForStatement('', [])));

		// second query corresponds with entry 1
		$this->assertSame($dataset[1], iterator_to_array($dataProvider->getDataForStatement('', [])));

		// all remaining queries are same as entry 1 since there are no more
		$this->assertSame($dataset[1], iterator_to_array($dataProvider->getDataForStatement('', [])));
	}

	/**
	 * @covers ::executeStatement
	 */
	public function testExecuteStatement()
	{
		$dataProvider = new SequentialArrayDataProvider([]);
		$this->assertSame(0, $dataProvider->executeStatement('', []));
	}

}