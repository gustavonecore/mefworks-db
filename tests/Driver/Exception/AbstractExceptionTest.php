<?php

use mef\Db\Driver\Exception\AbstractSqlException;

/**
 * @coversDefaultClass mef\Db\Driver\Exception\AbstractSqlException
 */
class AbstractExceptionTest extends PHPUnit_Framework_TestCase
{
	/**
	 * @covers ::__construct
	 * @covers ::getSql
	 * @covers ::getDriverMessage
	 */
	public function testAccessors()
	{
		$sql = 'SELECT';
		$driverMessage = 'error';

		$exception = $this->getMockForAbstractClass(
			AbstractSqlException::class,
			['Abstract Test: ', $sql, $driverMessage]
		);

		$this->assertSame($sql, $exception->getSql());
		$this->assertSame($driverMessage, $exception->getDriverMessage());
	}
}