<?php

use mef\Db\Driver\Exception\ExecuteException;

/**
 * @coversDefaultClass mef\Db\Driver\Exception\ExecuteException
 */
class ExecuteExceptionTest extends PHPUnit_Framework_TestCase
{
	/**
	 * @covers ::__construct
	 * @covers ::getSql
	 * @covers ::getDriverMessage
	 */
	public function testAccessors()
	{
		$sql = 'SELECT';
		$driverMessage = 'error';

		$exception = new ExecuteException($sql, $driverMessage);

		$this->assertSame($sql, $exception->getSql());
		$this->assertSame($driverMessage, $exception->getDriverMessage());
	}
}