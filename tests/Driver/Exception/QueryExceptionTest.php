<?php

use mef\Db\Driver\Exception\QueryException;

/**
 * @coversDefaultClass mef\Db\Driver\Exception\QueryException
 */
class QueryExceptionTest extends PHPUnit_Framework_TestCase
{
	/**
	 * @covers ::__construct
	 * @covers ::getSql
	 * @covers ::getDriverMessage
	 */
	public function testAccessors()
	{
		$sql = 'SELECT';
		$driverMessage = 'error';

		$exception = new QueryException($sql, $driverMessage);

		$this->assertSame($sql, $exception->getSql());
		$this->assertSame($driverMessage, $exception->getDriverMessage());
	}
}