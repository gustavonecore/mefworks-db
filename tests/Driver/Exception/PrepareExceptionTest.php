<?php

use mef\Db\Driver\Exception\PrepareException;

/**
 * @coversDefaultClass mef\Db\Driver\Exception\PrepareException
 */
class PrepareExceptionTest extends PHPUnit_Framework_TestCase
{
	/**
	 * @covers ::__construct
	 * @covers ::getSql
	 * @covers ::getDriverMessage
	 */
	public function testAccessors()
	{
		$sql = 'SELECT';
		$driverMessage = 'error';

		$exception = new PrepareException($sql, $driverMessage);

		$this->assertSame($sql, $exception->getSql());
		$this->assertSame($driverMessage, $exception->getDriverMessage());
	}
}