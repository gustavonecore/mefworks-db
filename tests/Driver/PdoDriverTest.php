<?php
use mef\Db\Driver\PdoDriver;
use mef\Db\RecordSet\RecordSetInterface;
use mef\Db\RecordSet\PdoRecordSet;
use mef\Db\Statement\StatementInterface;
use mef\Db\Statement\PdoStatement;

/**
 * @coversDefaultClass mef\Db\Driver\PdoDriver
 */
class PdoDriverTest extends PHPUnit_Framework_TestCase
{
	public function setUp()
	{
		$this->pdo = new PDO('sqlite::memory:');
		$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$this->driver = new PdoDriver($this->pdo);
		$this->driver->execute('CREATE TABLE test (k INTEGER, v TEXT)');
	}

	/**
	 * @covers ::__construct
	 */
	public function testConstructor()
	{
		$this->assertTrue($this->driver instanceof PdoDriver);
	}

	/**
	 * @covers ::getPDO
	 */
	public function testPDOGetter()
	{
		$this->assertEquals($this->pdo, $this->driver->getPDO());
	}

	/**
	 * @covers ::query
	 */
	public function testQuery()
	{
		$query = $this->driver->query('SELECT * FROM test');

		$this->assertTrue($query instanceof RecordSetInterface);
		$this->assertTrue($query instanceof PdoRecordSet);
	}

	/**
	 * @covers ::query
	 * @covers ::getErrorInfoString
	 *
	 * @expectedException mef\Db\Driver\Exception\QueryException
	 */
	public function testQueryException()
	{
		$this->driver->query('SELECT * FROM test2');
	}

	/**
	 * The call to execute() should return 0 because no rows are affected by
	 * the query.
	 *
	 * @covers ::execute
	 */
	public function testExecute()
	{
		$rows = $this->driver->execute('UPDATE test SET k=1 WHERE k=0');

		$this->assertSame(0, $rows);
	}

	/**
	 * @covers ::execute
	 * @covers ::getErrorInfoString
	 *
	 * @expectedException mef\Db\Driver\Exception\ExecuteException
	 */
	public function testExecuteException()
	{
		$this->driver->execute('UPDATE test2 SET k=1 WHERE k=0');
	}

	/**
	 * @covers ::prepare
	 */
	public function testPrepare()
	{
		$st = $this->driver->prepare('SELECT * FROM test WHERE k=?');

		$this->assertTrue($st instanceof StatementInterface);
		$this->assertTrue($st instanceof PdoStatement);
	}

	/**
	 * @covers ::prepare
	 * @covers ::getErrorInfoString
	 *
	 * @expectedException mef\Db\Driver\Exception\PrepareException
	 */
	public function testPrepareException()
	{
		$this->driver->prepare('SELECT * FROM test2 WHERE k=?');
	}

	/**
	 * The quoted value shouldn't be the same due to the apostrophe.
	 *
	 * @covers ::quoteValue
	 */
	public function testQuoteValue()
	{
		$value = 'Matthew\'s Test';
		$quotedValue = $this->driver->quoteValue($value);

		$this->assertTrue(is_string($quotedValue));
		$this->assertNotSame($quotedValue, $value);
	}
}