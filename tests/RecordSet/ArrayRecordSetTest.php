<?php

use mef\Db\RecordSet\ArrayRecordSet;

/**
 * @coversDefaultClass mef\Db\RecordSet\ArrayRecordSet
 */
class ArrayRecordSetTest extends PHPUnit_Framework_TestCase
{
	/**
	 * @covers ::__construct
	 */
	public function testConstructor()
	{
		$rs = new ArrayRecordSet(['foo']);

		$this->assertSame(1, count($rs));
	}
}