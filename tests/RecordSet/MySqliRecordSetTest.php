<?php

use mef\Db\RecordSet\MySqliRecordSet;

/**
 * @coversDefaultClass mef\Db\RecordSet\MySqliRecordSet
 */
class MySqliRecordSetTest extends PHPUnit_Framework_TestCase
{
	public function setUp()
	{
		$this->result = $this->getMockBuilder('mysqli_result')->
			disableOriginalConstructor()->
			getMock();

		$this->rs = new MySqliRecordSet($this->result);
	}

	/**
	 * @covers ::__construct
	 * @covers ::getMySqliResult
	 */
	public function testAccessors()
	{
		$this->assertSame($this->result, $this->rs->getMySqliResult());
	}

	/**
	 * @covers ::close
	 */
	public function testClose()
	{
		$this->result->expects($this->once())->method('free');

		$this->rs->close();
		$this->assertNull($this->rs->getMySqliResult());
	}

	/**
	 * @covers ::count
	 */
	public function testCount()
	{
		// MySQLi will throw a warning here because we haven't actually ran a
		// query.
		@$this->assertSame(0, $this->rs->count());
		@$this->assertSame(0, count($this->rs));
	}

	/**
	 * @covers ::fetchRow
	 */
	public function testFetchRow()
	{
		$data = ['key' => 'val'];

		$this->result->expects($this->at(0))->
			method('fetch_assoc')->
			will($this->returnValue($data));

		$this->result->expects($this->at(1))->
			method('fetch_assoc')->
			will($this->returnValue(null));

		$this->assertSame($data, $this->rs->fetchRow());
		$this->assertSame([], $this->rs->fetchRow());
	}

	/**
	 * @covers ::fetchRowAsArray
	 */
	public function testFetchRowAsArray()
	{
		$data = [0 => 'val'];

		$this->result->expects($this->at(0))->
			method('fetch_row')->
			will($this->returnValue($data));

		$this->result->expects($this->at(1))->
			method('fetch_row')->
			will($this->returnValue(null));

		$this->assertSame($data, $this->rs->fetchRowAsArray());
		$this->assertSame([], $this->rs->fetchRowAsArray());
	}
}