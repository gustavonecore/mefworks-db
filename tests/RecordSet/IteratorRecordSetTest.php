<?php

use mef\Db\RecordSet\IteratorRecordSet;

/**
 * @coversDefaultClass mef\Db\RecordSet\IteratorRecordSet
 */
class IteratorRecordSetTest extends PHPUnit_Framework_TestCase
{
	public function setUp()
	{
		$this->rs = new IteratorRecordSet(new ArrayIterator([['id' => '1']]));
	}

	/**
	 * @covers ::__construct
	 */
	public function testConstructor()
	{
		$this->assertSame(1, count($this->rs));
	}

	/**
	 * @covers ::close
	 */
	public function testClose()
	{
		$this->rs->close();

		$this->assertSame([], $this->rs->fetchRow());
	}

	/**
	 * @covers ::fetchRow
	 */
	public function testFetchRow()
	{
		$this->assertSame(['id' => '1'], $this->rs->fetchRow());
		$this->assertSame([], $this->rs->fetchRow());

		$this->rs->close();
		$this->assertSame([], $this->rs->fetchRow());
	}

	/**
	 * @covers ::fetchRowAsArray
	 */
	public function testFetchRowAsArray()
	{
		$this->assertSame([0 => '1'], $this->rs->fetchRowAsArray());
		$this->assertSame([], $this->rs->fetchRowAsArray());

		$this->rs->close();
		$this->assertSame([], $this->rs->fetchRowAsArray());
	}

	/**
	 * @covers ::count
	 */
	public function testCount()
	{
		$this->assertSame(1, count($this->rs));

		$rs = new IteratorRecordSet($this->getMock('Iterator'));
		$this->assertSame(0, count($rs));
	}
}