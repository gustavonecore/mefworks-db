<?php

use mef\Db\Driver\AbstractDriver;
use mef\Db\RecordSet\AbstractRecordSet;

/**
 * @coversDefaultClass mef\Db\RecordSet\AbstractRecordSet
 */
class AbstractRecordSetTest extends PHPUnit_Framework_TestCase
{
	public function setUp()
	{
		$this->driver = new DriverB;
	}

	protected function getRecordSet($dataset)
	{
		$dataset[] = [];

		$rs = $this->getMockForAbstractClass(AbstractRecordSet::class);

		foreach ($dataset as $i => $data)
		{
			$rs->expects($this->at($i))->
				method('fetchRow')->
				will($this->returnValue($data));
		}

		return $rs;
	}

	protected function getRecordSetAsArray($dataset)
	{
		$dataset[] = [];

		$rs = $this->getMockForAbstractClass(AbstractRecordSet::class);

		foreach ($dataset as $i => $data)
		{
			$rs->expects($this->at($i))->
				method('fetchRowAsArray')->
				will($this->returnValue($data));
		}

		return $rs;
	}

	/**
	 * @covers ::fetchValue
	 * @covers ::__destruct
	 */
	public function testFetchValue()
	{
		$rs = new RecordSetB([
			['id' => '42', 'val' => 'foo'],
			['id' => '12', 'val' => 'bar'],
		]);

		$this->assertSame('42', $rs->fetchValue());
		$this->assertSame('bar', $rs->fetchValue(1));
		$this->assertNull($rs->fetchValue());
	}

	/**
	 * @covers ::fetchRowInto
	 */
	public function testFetchRowInto()
	{
		$rs = new RecordSetB([
			['id' => '42', 'val' => 'foo']
		]);

		$array = ['extra' => null];

		$this->assertTrue($rs->fetchRowInto($array));

		$this->assertEquals($array, [
			'id' => 42,
			'val' => 'foo',
			'extra' => null
		]);

		$this->assertFalse($rs->fetchRowInto($array));
	}

	/**
	 * @covers ::fetchRowIntoArray
	 */
	public function testFetchRowIntoArray()
	{
		$rs = new RecordSetB([
			['id' => '42', 'val' => 'foo']
		]);

		$array = [0 => null, 4 => null];

		$this->assertTrue($rs->fetchRowIntoArray($array));

		$this->assertEquals($array, [
			0 => 42,
			1 => 'foo',
			4 => null
		]);

		$this->assertFalse($rs->fetchRowIntoArray($array));
	}

	/**
	 * @covers ::fetchRowIntoObject
	 */
	public function testFetchRowIntoObject()
	{
		$rs = new RecordSetB([
			['id' => '42', 'val' => 'foo']
		]);

		$object = new StdClass;
		$object->extra = 'bar';

		$this->assertTrue($rs->fetchRowIntoObject($object));

		$this->assertSame('42', $object->id);
		$this->assertSame('foo', $object->val);
		$this->assertSame('bar', $object->extra);

		$this->assertFalse($rs->fetchRowIntoObject($object));
	}


	/**
	 * @covers ::fetchAll
	 * @covers ::getIterator
	 */
	public function testFetchAll()
	{
		$this->assertSame(
			[
				['k' => '1', 'v' => 'one'],
				['k' => '2', 'v' => 'two'],
			],
			$this->driver->query('')->fetchAll()
		);
	}

	/**
	 * @covers ::fetchKeyed
	 * @covers ::getKeyedIterator
	 */
	public function testFetchWithKey()
	{
		$this->assertSame(
			[
				'1' => 'one',
				'2' => 'two',
			],
			$this->driver->query('')->fetchKeyed()
		);

		$this->assertSame(
			[
				'one' => '1',
				'two' => '2',
			],
			$this->driver->query('')->fetchKeyed('v')
		);

		$this->assertSame(
			[
				'1' => ['v' => 'one', 'e' => null],
				'2' => ['v' => 'two', 'e' => null],
			],
			$this->driver->query('x')->fetchKeyed('k')
		);
	}

	/**
	 * @covers ::fetchKeyed
	 * @covers ::getKeyedIterator
	 */
	public function testFetchKeyedWithOneColumn()
	{
		$rs = $this->getRecordSet([['id' => 'a'], ['id' => 'b']]);
		$this->assertSame(['a' => true, 'b' => true], $rs->fetchKeyed());
	}

	/**
	 * @covers ::getKeyedIterator
	 *
	 * @expectedException InvalidArgumentException
	 */
	public function testInvalidKeyedIterator()
	{
		$this->driver->query('')->fetchKeyed('invalid');
	}

	/**
	 * @covers ::fetchAllAsArray
	 * @covers ::getArrayIterator
	 */
	public function testFetchAllAsArray()
	{
		$this->assertSame(
			[
				['1', 'one'],
				['2', 'two'],
			],
			$this->driver->query('')->fetchAllAsArray()
		);
	}

	/**
	 * @covers ::fetchKeyedAsArray
	 * @covers ::getKeyedArrayIterator
	 */
	public function testFetchArrayWithKey()
	{
		$this->assertSame(
			[
				'1' => 'one',
				'2' => 'two',
			],
			$this->driver->query('')->fetchKeyedAsArray()
		);

		$this->assertSame(
			[
				'one' => '1',
				'two' => '2',
			],
			$this->driver->query('')->fetchKeyedAsArray(1)
		);

		$this->assertSame(
			[
				'1' => ['one', null],
				'2' => ['two', null],
			],
			$this->driver->query('x')->fetchKeyedAsArray()
		);
	}

	/**
	 * @covers ::fetchKeyedAsArray
	 * @covers ::getKeyedArrayIterator
	 */
	public function testFetchKeyedArrayWithOneColumn()
	{
		$rs = $this->getRecordSetAsArray([[0 => 'a'], [0 => 'b']]);
		$this->assertSame(['a' => true, 'b' => true], $rs->fetchKeyedAsArray());
	}

	/**
	 * @covers ::getKeyedArrayIterator
	 *
	 * @expectedException InvalidArgumentException
	 */
	public function testInvalidKeyedArrayIterator()
	{
		$this->driver->query('')->fetchKeyedAsArray(42);
	}

	/**
	 * @covers ::getKeyedArrayIterator
	 *
	 * @expectedException InvalidArgumentException
	 */
	public function testInvalidKeyedArrayIteratorOnBoundary()
	{
		// The driver uses 2 columns, so since it is 0-based, this is invalid.
		$this->driver->query('')->fetchKeyedAsArray(2);
	}


	/**
	 * @covers ::fetchAllWithCallback
	 * @covers ::getCallbackIterator
	 */
	public function testFetchAllWithCallback()
	{
		$this->assertSame(
			[
				['1', 'one'],
				['2', 'two'],
			],
			$this->driver->query('')->fetchAllWithCallback(function($row) {
				return array_values($row);
			})
		);
	}

	/**
	 * @covers ::fetchKeyedWithCallback
	 * @covers ::getKeyedCallbackIterator
	 */
	public function testFetchCallbackWithKey()
	{
		$this->assertSame(
			[
				'1' => 'one',
				'2' => 'two',
			],
			$this->driver->query('')->fetchKeyedWithCallback(function($row) {
				return [$row['k'], $row['v']];
			})
		);

		$this->assertSame(
			[
				'one' => '1',
				'two' => '2',
			],
			$this->driver->query('')->fetchKeyedWithCallback(function($row) {
				return [$row['v'], $row['k']];
			})
		);

		$this->assertSame(
			[
				'1' => ['one', null],
				'2' => ['two', null],
			],
			$this->driver->query('x')->fetchKeyedWithCallback(function($row) {
				return [$row['k'], [$row['v'], $row['e']]];
			})
		);
	}

	/**
	 * @covers ::fetchColumn
	 * @covers ::getColumnIterator
	 */
	public function testFetchColumn()
	{
		$this->assertSame(['1', '2'], $this->driver->query('')->fetchColumn());
		$this->assertSame(['one', 'two'], $this->driver->query('')->fetchColumn(1));
	}
}

class DriverB extends AbstractDriver
{
	public function query($sql)
	{
		if ($sql === '')
		{
			$data = [
				['k' => '1', 'v' => 'one'],
				['k' => '2', 'v' => 'two'],
			];
		}
		else
		{
			$data = [
				['k' => '1', 'v' => 'one', 'e' => null],
				['k' => '2', 'v' => 'two', 'e' => null],
			];
		}

		return new RecordSetB($data);
	}

	public function execute($sql)
	{
		return 0;
	}

	public function prepare($sql, array $params = [])
	{
		throw new Exception('Not Implemented');
	}

	public function quoteValue($value)
	{
		return $value;
	}
}

class RecordSetB extends AbstractRecordSet
{
	protected $iterator;

	public function __construct(array $data)
	{
		$this->iterator = new ArrayIterator($data);
	}

	public function close()
	{
		$this->iterator = null;
	}

	public function count()
	{
		return 0;
	}

	public function fetchRow()
	{
		$row = $this->iterator->current();
		$this->iterator->next();
		return $row ?: [];
	}

	public function fetchRowAsArray()
	{
		$row = $this->iterator->current();
		$this->iterator->next();
		return array_values($row ?: []);
	}
}