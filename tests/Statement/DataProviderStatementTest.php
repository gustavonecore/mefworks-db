<?php

use mef\Db\Statement\DataProviderStatement;
use mef\Db\Driver\DataProvider\DataProviderInterface;
use mef\Db\RecordSet\RecordSetInterface;

/**
 * @coversDefaultClass mef\Db\Statement\DataProviderStatement
 */
class DataProviderStatementTest extends PHPUnit_Framework_TestCase
{
	public function setUp()
	{
		$this->dataProvider = $this->getMock(DataProviderInterface::class);
		$this->sql = 'SELECT';
		$this->parameters = [1,2,3];

		$this->statement = new DataProviderStatement($this->dataProvider, $this->sql, $this->parameters);
	}

	/**
	 * @covers ::__construct
	 * @covers ::getDataProvider
	 * @covers ::getSql
	 * @covers ::getParameters
	 */
	public function testConstructor()
	{
		$this->assertSame($this->dataProvider, $this->statement->getDataProvider());
		$this->assertSame($this->sql, $this->statement->getSql());
		$this->assertSame($this->parameters, $this->statement->getParameters());
	}

	/**
	 * @covers ::bindParameter
	 */
	public function testBindParameter()
	{
		$this->statement->bindParameter(':key', $value);
		$this->assertNull($this->statement->getParameters()[':key']);

		$value = 'bar';
		$this->assertSame('bar', $this->statement->getParameters()[':key']);
	}

	/**
	 * @covers ::bindParameters
	 */
	public function testBindParameters()
	{
		$this->statement->bindParameters([':key' => &$value]);
		$this->assertNull($this->statement->getParameters()[':key']);

		$value = 'bar';
		$this->assertSame('bar', $this->statement->getParameters()[':key']);
	}

	/**
	 * @covers ::setParameter
	 */
	public function testSetParameter()
	{
		$this->statement->setParameter(':key', 'bar');
		$this->assertSame('bar', $this->statement->getParameters()[':key']);
	}

	/**
	 * @covers ::setParameters
	 */
	public function testSetParameters()
	{
		$this->statement->setParameters([':key' => 'bar']);
		$this->assertSame('bar', $this->statement->getParameters()[':key']);
	}

	/**
	 * @covers ::query
	 */
	public function testQuery()
	{
		$this->dataProvider->expects($this->once())->
			method('getDataForStatement')->
			with($this->sql, $this->parameters)->
			will($this->returnValue($this->getMock('Iterator')));

		$this->assertTrue($this->statement->query() instanceof RecordSetInterface);
	}

	/**
	 * @covers ::execute
	 */
	public function testExecute()
	{
		$this->dataProvider->expects($this->once())->
			method('executeStatement')->
			with($this->sql, $this->parameters)->
			will($this->returnValue(42));

		$this->assertSame(42, $this->statement->execute());
	}
}