<?php

use PDOStatement as PHP_PDOStatement;
use mef\Db\Statement\PdoStatement;
use mef\Db\RecordSet\PdoRecordSet;

/**
 * @coversDefaultClass mef\Db\Statement\PDOStatement
 */
class PdoStatementTest extends PHPUnit_Framework_TestCase
{
	public function setUp()
	{
		$this->pdo = $this->getMock(PHP_PDOStatement::class);
		$this->st = new PDOStatement($this->pdo);
	}

	/**
	 * @covers ::__construct
	 * @covers ::getPdoStatement
	 */
	public function testAccessors()
	{
		$this->assertSame($this->pdo, $this->st->getPdoStatement());
	}

	/**
	 * @covers ::bindParameter
	 */
	public function testBindParam()
	{
		$this->pdo->expects($this->once())->method('bindParam')->with(':key');

		$this->st->bindParameter(':key', $value);
	}

	/**
	 * @covers ::setParameter
	 */
	public function testBindValue()
	{
		$this->pdo->expects($this->once())->method('bindValue')->with(':key');

		$this->st->setParameter(':key', 'value');
	}

	/**
	 * @covers ::bindParameter
	 */
	public function testBindIndexedParam()
	{
		// PDO expects 1-based parameters, so passing 0 should give us 1
		$this->pdo->expects($this->once())->method('bindParam')->with(1);

		$this->st->bindParameter(0, $value);
	}

	/**
	 * @covers ::setParameter
	 */
	public function testBindIndexedValue()
	{
		// PDO expects 1-based parameters, so passing 0 should give us 1
		$this->pdo->expects($this->once())->method('bindValue')->with(1);

		$this->st->setParameter(0, 'value');
	}

	/**
	 * @covers ::execute
	 */
	public function testExecute()
	{
		$this->pdo->expects($this->once())->method('execute');
 		$this->pdo->expects($this->once())->method('rowCount')->will($this->returnValue(42));

		$this->assertSame(42, $this->st->execute());
	}

	/**
	 * @covers ::query
	 */
	public function testQuery()
	{
		$this->pdo->expects($this->once())->method('execute');

		$this->assertTrue($this->st->query() instanceof PDORecordSet);
	}
}