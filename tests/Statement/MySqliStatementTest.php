<?php

use mef\Db\Statement\MySqliStatement;
use mef\Db\RecordSet\MySqliRecordSet;

/**
 * @coversDefaultClass mef\Db\Statement\MySqliStatement
 */
class MySqliStatementTest extends PHPUnit_Framework_TestCase
{
	/**
	 * @covers ::__construct
	 * @covers ::getMySQliStatement
	 * @covers ::getNamedParameters
	 * @covers ::getBoundParameters
	 */
	public function testAccessors()
	{
		$mysqli_stmt = $this->getMockBuilder('mysqli_stmt')->
			disableOriginalConstructor()->
			getMock();

		$st = new MySqliStatement($mysqli_stmt, []);

		$this->assertSame($mysqli_stmt, $st->getMySqliStatement());
		$this->assertSame([], $st->getNamedParameters());
		$this->assertSame([], $st->getBoundParameters());
	}

	/**
	 * @covers ::bindParameter
	 */
	public function testBindNamedParam()
	{
		$mysqli_stmt = $this->getMockBuilder('mysqli_stmt')->
			disableOriginalConstructor()->
			getMock();

		$val = 'Hello';

		$st = new MySqliStatement($mysqli_stmt, [':key' => [0]]);
		$st->bindParameter(':key', $val);

		$this->assertSame([0 => $val], $st->getBoundParameters());
	}

	/**
	 * @covers ::bindParameter
	 */
	public function testBindIndexedParam()
	{
		$mysqli_stmt = $this->getMockBuilder('mysqli_stmt')->
			disableOriginalConstructor()->
			getMock();

		$val = 'Hello';

		$st = new MySqliStatement($mysqli_stmt, []);
		$st->bindParameter(0, $val);

		$this->assertSame([0 => $val], $st->getBoundParameters());
	}

	/**
	 * @covers ::bindParameter
	 *
	 * @expectedException InvalidArgumentException
	 */
	public function testBindInvalidNamedParam()
	{
		$mysqli_stmt = $this->getMockBuilder('mysqli_stmt')->
			disableOriginalConstructor()->
			getMock();

		$val = 'Hello';

		$st = new MySqliStatement($mysqli_stmt, [':key' => [0]]);
		$st->bindParameter(':invalid', $val);
	}


	/**
	 * @covers ::bindParameter
	 *
	 * @expectedException InvalidArgumentException
	 */
	public function testBindInvalidParamType()
	{
		$mysqli_stmt = $this->getMockBuilder('mysqli_stmt')->
			disableOriginalConstructor()->
			getMock();

		$val = 'Hello';

		$st = new MySqliStatement($mysqli_stmt, [':key' => [0]]);
		$st->bindParameter(null, $val);
	}

	/**
	 * @covers ::setParameter
	 */
	public function testBindValue()
	{
		$mysqli_stmt = $this->getMockBuilder('mysqli_stmt')->
			disableOriginalConstructor()->
			getMock();

		$val = 'Hello';

		$st = new MySqliStatement($mysqli_stmt, []);
		$st->setParameter(0, $val);
	}

	/**
	 * @covers ::query
	 */
	public function testQuery()
	{
		$mysqli_stmt = $this->getMockBuilder('mysqli_stmt')->
			disableOriginalConstructor()->
			getMock();

		$mysqli_result = $this->getMockBuilder('mysqli_result')->
			disableOriginalConstructor()->
			getMock();

		$mysqli_stmt->expects($this->once())->method('bind_param');
		$mysqli_stmt->expects($this->once())->method('execute');

		$mysqli_stmt->method('get_result')->willReturn($mysqli_result);

		$st = new MySqliStatement($mysqli_stmt, []);
		$st->setParameter(0, 'Hello');

		$this->assertTrue($st->query() instanceof MySqliRecordSet);
	}

	/**
	 * @covers ::query
	 */
	public function testQueryNoParameters()
	{
		$mysqli_stmt = $this->getMockBuilder('mysqli_stmt')->
			disableOriginalConstructor()->
			getMock();

		$mysqli_result = $this->getMockBuilder('mysqli_result')->
			disableOriginalConstructor()->
			getMock();

		$mysqli_stmt->expects($this->never())->method('bind_param');
		$mysqli_stmt->expects($this->once())->method('execute');

		$mysqli_stmt->method('get_result')->willReturn($mysqli_result);

		$st = new MySqliStatement($mysqli_stmt, []);
		$st->query();
	}

	/**
	 * @covers ::execute
	 */
	public function testExecute()
	{
		$mysqli_stmt = $this->getMockBuilder('mysqli_stmt')->
			disableOriginalConstructor()->
			getMock();

		$mysqli_stmt->expects($this->once())->method('bind_param');
		$mysqli_stmt->expects($this->once())->method('execute');

		$st = new MySqliStatement($mysqli_stmt, []);
		$st->setParameter(0, 'Hello');
		@$st->execute();
	}

	/**
	 * @covers ::execute
	 */
	public function testExecuteNoParameters()
	{
		$mysqli_stmt = $this->getMockBuilder('mysqli_stmt')->
			disableOriginalConstructor()->
			getMock();

		$mysqli_stmt->expects($this->never())->method('bind_param');
		$mysqli_stmt->expects($this->once())->method('execute');

		$st = new MySqliStatement($mysqli_stmt, []);
		@$st->execute();
	}
}