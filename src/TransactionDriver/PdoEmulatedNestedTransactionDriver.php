<?php namespace mef\Db\TransactionDriver;

use InvalidArgumentException;

use mef\Db\Driver\DriverInterface;
use mef\Db\Driver\PdoDriver;

class PdoEmulatedNestedTransactionDriver extends EmulatedNestedTransactionDriver
{
	/**
	 * @var \mef\Db\Driver\PdoDriver
	 */
	private $pdoDriver;

	/**
	 * {@inheritdoc}
	 */
	public function setDatabaseDriver(DriverInterface $db)
	{
		if ($db instanceof PdoDriver === false)
		{
			throw new InvalidArgumentException('db must be a ' . PdoDriver::class);
		}

		parent::setDatabaseDriver($db);
		$this->pdoDriver = $db;
	}

	/**
	 * Start the transaction.
	 */
	protected function doStart()
	{
		return $this->pdoDriver->getPdo()->beginTransaction();
	}

	/**
	 * Commit the transaction.
	 */
	protected function doCommit()
	{
		return $this->pdoDriver->getPdo()->commit();
	}

	/**
	 * Roll back the transaction.
	 */
	protected function doRollBack()
	{
		return $this->pdoDriver->getPdo()->rollBack();
	}
}