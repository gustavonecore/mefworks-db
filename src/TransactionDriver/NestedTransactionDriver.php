<?php namespace mef\Db\TransactionDriver;

use mef\Db\TransactionDriver\Exception\TransactionNotStartedException;

/**
 * Nested transactions are fully implemented using save points.
 *
 * The syntax can be altered from the default SQL compliant values by setting
 * any one of the *syntax properties.
 */
class NestedTransactionDriver extends AbstractTransactionDriver
{
	private $stack = [];

	/**
	 * Begin a transaction.
	 *
	 * If a transaction has already been started, then a save point is
	 * created.
	 *
	 * @param string|null $name  The name of the transaction. Specifying a
	 *                           name is optional, and in fact, does nothing
	 *                           important as there is no way to reference it
	 *                           later on.
	 */
	public function start($name = null)
	{
		if (count($this->stack) === 0)
		{
			$this->db->execute($this->getStartSyntax());
			$this->stack[] = null;
		}
		else
		{
			$name = $name ?: '_sp_' . count($this->stack);
			$this->db->execute($this->getSavePointSyntax($name));
			$this->stack[] = $name;
		}
	}

	/**
	 * Commits the most recently opened transaction.
	 *
	 * If the active transaction is a save point, then it is released.
	 * Changes will not be persisted until after the outermost (first)
	 * transaction is committed.
	 */
	public function commit()
	{
		$c = count($this->stack);

		if ($c === 0)
		{
			throw new TransactionNotStartedException;
		}
		else if ($c === 1)
		{
			$this->stack = [];
			$this->db->execute($this->getCommitSyntax());
		}
		else
		{
			$name = array_pop($this->stack);
			$this->db->execute($this->getReleaseSavePointSyntax($name));
		}
	}

	/**
	 * Rolls back the most recently opened transaction.
	 */
	public function rollBack()
	{
		$c = count($this->stack);

		if ($c === 0)
		{
			throw new TransactionNotStartedException;
		}
		else if ($c === 1)
		{
			$this->stack = [];
			$this->db->execute($this->getRollBackSyntax());
		}
		else
		{
			$name = array_pop($this->stack);
			$this->db->execute($this->getRollBackSavePointSyntax($name));
		}
	}

	public function getDepth()
	{
		return count($this->stack);
	}

	protected function getStartSyntax()
	{
		return 'BEGIN';
	}

	protected function getRollBackSyntax()
	{
		return 'ROLLBACK';
	}

	protected function getCommitSyntax()
	{
		return 'COMMIT';
	}

	protected function getSavePointSyntax($name)
	{
		return 'SAVEPOINT ' . $name;
	}

	protected function getReleaseSavePointSyntax($name)
	{
		return 'RELEASE SAVEPOINT ' . $name;
	}

	protected function getRollBackSavePointSyntax($name)
	{
		return 'ROLLBACK TO SAVEPOINT ' . $name;
	}

}