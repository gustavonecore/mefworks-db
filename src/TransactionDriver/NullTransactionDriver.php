<?php namespace mef\Db\TransactionDriver;

use mef\Db\TransactionDriver\Exception\TransactionNotStartedException;

/**
 * This does absolutely nothing.
 *
 * The database driver is never informed of the transactions.
 */
class NullTransactionDriver implements TransactionDriverInterface
{
	/**
	 * @var integer
	 */
	protected $depth = 0;

	/**
	 * Start a transaction.
	 *
	 * @param  string $name
	 */
	public function start($name = '')
	{
		++$this->depth;
	}

	/**
	 * Commit the transaction.
	 */
	public function commit()
	{
		if ($this->depth === 0)
		{
			throw new TransactionNotStartedException;
		}

		--$this->depth;
	}

	/**
	 * Roll back the transaction.
	 */
	public function rollBack()
	{
		if ($this->depth === 0)
		{
			throw new TransactionNotStartedException;
		}

		--$this->depth;
	}

	/**
	 * Return the number of open transactions
	 *
	 * @return integer
	 */
	public function getDepth()
	{
		return $this->depth;
	}
}