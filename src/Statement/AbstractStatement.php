<?php namespace mef\Db\Statement;

use mef\Db\Statement;

abstract class AbstractStatement implements StatementInterface
{
	public function bindParameters(array $parameters, array $types = [])
	{
		foreach ($parameters as $key => &$value)
		{
			$this->bindParameter($key, $value, isset($types[$key]) ? $types[$key] : Statement::AUTOMATIC);
		}
	}

	public function setParameters(array $parameters, array $types = [])
	{
		foreach ($parameters as $key => $value)
		{
			$this->setParameter($key, $value, isset($types[$key]) ? $types[$key] : Statement::AUTOMATIC);
		}
	}
}
