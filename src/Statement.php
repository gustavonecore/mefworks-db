<?php namespace mef\Db;

/**
 * @codeCoverageIgnore
 */
final class Statement
{
	const AUTOMATIC = 0;
	const NULL = 1;
	const BOOLEAN = 2;
	const INTEGER = 3;
	const STRING = 4;
	const BLOB = 5;

	final private function __construct()
	{
	}
}