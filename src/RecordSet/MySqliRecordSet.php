<?php namespace mef\Db\RecordSet;

use mysqli_result;

/**
 * A recordset for use with the MySqliDriver
 */
class MySqliRecordSet extends AbstractRecordSet
{
	/**
	 * @var mysqli_result
	 */
	protected $result;

	/**
	 * Constructor
	 *
	 * @param mysqli_result $result
	 */
	public function __construct(mysqli_result $result)
	{
		$this->result = $result;
	}

	/**
	 * Return the underlying mysqli_result
	 *
	 * @return mysqli_result
	 */
	public function getMySqliResult()
	{
		return $this->result;
	}

	/**
	 * Close the recordset. It cannot be used after this.
	 */
	public function close()
	{
		if ($this->result)
		{
			$this->result->free();
			$this->result = null;
		}
	}

	/**
	 * Return the number of rows in the recordset.
	 *
	 * @return integer
	 */
	public function count()
	{
		return $this->result->num_rows ?: 0;
	}

	/**
	 * Fetch the next row as an associative array.
	 *
	 * @return array
	 */
	public function fetchRow()
	{
		return $this->result->fetch_assoc() ?: [];
	}

	/**
	 * Fetch the next row as an indexed array.
	 *
	 * @return array
	 */
	public function fetchRowAsArray()
	{
		return $this->result->fetch_row() ?: [];
	}
}