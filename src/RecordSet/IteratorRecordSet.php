<?php namespace mef\Db\RecordSet;

use Countable;
use Iterator;
use mef\Db\RecordSet\AbstractRecordSet;

/**
 * Convert any iterator into a recordset.
 */
class IteratorRecordSet extends AbstractRecordSet
{
	/**
	 * @var Iterator
	 */
	private $iterator;

	/**
	 * @var boolean
	 */
	private $closed = false;

	/**
	 * Constructor
	 *
	 * The iterator must return one associative array per row.
	 *
	 * @param Iterator $iterator
	 */
	public function __construct(Iterator $iterator)
	{
		$this->iterator = $iterator;
	}

	/**
	 * Close the recordset, making it invalid.
	 */
	public function close()
	{
		$this->closed = true;
	}

	/**
	 * Return the next row as an associative array.
	 *
	 * Return [] if there are no more rows.
	 *
	 * @return array
	 */
	public function fetchRow()
	{
		if ($this->closed)
		{
			return [];
		}
		else
		{
			$row = $this->iterator->current();
			$this->iterator->next();

			return $row ?: [];
		}
	}

	/**
	 * Return the next row as an indexed array.
	 *
	 * Return [] if there are no more rows.
	 *
	 * @return array
	 */
	public function fetchRowAsArray()
	{
		if ($this->closed)
		{
			return [];
		}
		else
		{
			$row = $this->iterator->current() ?: [];
			$this->iterator->next();

			return array_values($row);
		}
	}

	/**
	 * Return the count of records, if supported.
	 *
	 * Will return 0 if it is unknown.
	 *
	 * @return integer
	 */
	public function count()
	{
		if ($this->iterator instanceof Countable)
		{
			return $this->iterator->count();
		}
		else
		{
			return 0;
		}
	}
}