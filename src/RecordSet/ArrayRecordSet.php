<?php namespace mef\Db\RecordSet;

use ArrayIterator;

/**
 * Convert any array into a recordset.
 */
class ArrayRecordSet extends IteratorRecordSet
{
	/**
	 * Constructor
	 *
	 * The array must be an array of associative arrays, with each array
	 * representing a single row.
	 *
	 * @param array $array
	 */
	public function __construct(array $array)
	{
		parent::__construct(new ArrayIterator($array));
	}
}