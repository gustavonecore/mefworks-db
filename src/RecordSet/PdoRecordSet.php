<?php namespace mef\Db\RecordSet;

use PDO;
use PDOStatement;

class PdoRecordSet extends AbstractRecordSet
{
	/**
	 * @var PDOStatement
	 */
	protected $st;

	/**
	 * Constructor
	 *
	 * @param PDOStatement $st
	 */
	public function __construct(PDOStatement $st)
	{
		$this->st = $st;
	}

	/**
	 * Return the underlying PDO Statement
	 *
	 * @return PDOStatement
	 */
	public function getPdoStatement()
	{
		return $this->st;
	}

	/**
	 * Close the statement. It will no longer be usable.
	 */
	public function close()
	{
		if ($this->st)
		{
			$this->st->closeCursor();
			$this->st = null;
		}
	}

	/**
	 * Return the number of rows.
	 *
	 * @return integer
	 */
	public function count()
	{
		return $this->st->rowCount();
	}

	/**
	 * Return the next row as an associative array.
	 *
	 * @return array
	 */
	public function fetchRow()
	{
		return $this->st->fetch(PDO::FETCH_ASSOC) ?: [];
	}

	/**
	 * Return the next row as an indexed array.
	 *
	 * @return array
	 */
	public function fetchRowAsArray()
	{
		return $this->st->fetch(PDO::FETCH_NUM) ?: [];
	}
}