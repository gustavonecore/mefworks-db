<?php namespace mef\Db\RecordSet;

use Countable;
use InvalidArgumentException;
use IteratorAggregate;

/**
 * An abstract implementation of `mef\Db\RecordSet\RecordSetInterface`.
 *
 * close, count, fetchRow, and fetchRowAsArray are left unimplemented.
 */
abstract class AbstractRecordSet implements RecordSetInterface,
	IteratorAggregate, Countable
{
	/**
	 * Destructor
	 */
	public function __destruct()
	{
		$this->close();
	}

	/**
	 * Fetch the next record and return a single value from it.
	 *
	 * By default the first column is used.
	 *
	 * Return null if no rows exist.
	 *
	 * @param integer $index  0-based column to use
	 *
	 * @return mixed
	 */
	public function fetchValue($index = 0)
	{
		$array = $this->fetchRowAsArray();

		return isset($array[$index]) ? $array[$index] : null;
	}

	/**
	 * Fetch the next row and replace the contents of the specificed
	 * associative array with it.
	 *
	 * Keys in the supplied array that do not exist in the row will
	 * not be modified.
	 *
	 * @param  array  &$array
	 *
	 * @return boolean  true if the array was updated
	 *                  false if there are no more rows
	 */
	public function fetchRowInto(array &$array)
	{
		$columns = $this->fetchRow();

		if (count($columns) === 0)
		{
			return false;
		}
		else
		{
			foreach ($columns as $key => $value)
			{
				$array[$key] = $value;
			}

			return true;
		}
	}

	/**
	 * Fetch the next row and replace the contents of the specificed
	 * indexed array with it.
	 *
	 * Indexes in the supplied array that do not exist in the row will
	 * not be modified.
	 *
	 * @param  array  &$array
	 *
	 * @return boolean  true if the array was updated
	 *                  false if there are no more rows
	 */
	public function fetchRowIntoArray(array &$array)
	{
		$columns = $this->fetchRowAsArray();

		if (count($columns) === 0)
		{
			return false;
		}
		else
		{
			foreach ($columns as $key => $value)
			{
				$array[$key] = $value;
			}

			return true;
		}
	}

	public function fetchRowIntoObject(&$object)
	{
		$columns = $this->fetchRow();

		if (count($columns) === 0)
		{
			return false;
		}
		else
		{
			foreach ($columns as $key => $value)
			{
				$object->$key = $value;
			}

			return true;
		}
	}

	/**
	 * Return the entire resultset as an indexed array of associative rows.
	 *
	 * @return array       An indexed array of associative rows
	 */
	public function fetchAll()
	{
		return iterator_to_array($this->getIterator());
	}

	/**
	 * Return the entire resultset as an associative array of associative
	 * rows.
	 *
	 * The specified key will be omitted from the row. If no key is supplied,
	 * then the first column is used.
	 *
	 * @param string $key  The column to use as the key.
	 *
	 * @return array       An associative array of associative rows
	 */
	public function fetchKeyed($key = '')
	{
		return iterator_to_array($this->getKeyedIterator($key));
	}

	/**
	 * Return the entire resultset as an indexed array of indexed rows.
	 *
	 * @return array       An indexed array of indexed rows
	 */
	public function fetchAllAsArray()
	{
		return iterator_to_array($this->getArrayIterator());
	}

	/**
	 * Return the entire resultset as an indexed array of values.
	 *
	 * @param integer $index  The 0-based column index to return
	 *
	 * @return array       An indexed array of indexed rows
	 */
	public function fetchColumn($index = 0)
	{
		return iterator_to_array($this->getColumnIterator($index));
	}

	/**
	 * Return the entire resultset as an associative array of indexed rows.
	 *
	 * The specified index will be omitted from the row. If no index is
	 * supplied, then the first column will be used.
	 *
	 * @param integer $index  The column index to use as the key.
	 *
	 * @return array       An indexed array of indexed rows
	 */
	public function fetchKeyedAsArray($index = 0)
	{
		return iterator_to_array($this->getKeyedArrayIterator($index));
	}

	/**
	 * Return the entire resultset as an indexed array, using the return value
	 * of the callback on each row.
	 *
	 * @param  callable $callback  A function to use to map the value
	 *
	 * @return array
	 */
	public function fetchAllWithCallback(callable $callback)
	{
		return iterator_to_array($this->getCallbackIterator($callback));
	}

	/**
	 * Return the entire resultset as an associative array, using the return
	 * value of the callback on each row.
	 *
	 * @param  callable $callback  A function to use to map the value
	 *
	 * @return array
	 */
	public function fetchKeyedWithCallback(callable $callback)
	{
		return iterator_to_array($this->getKeyedCallbackIterator($callback));
	}

	/**
	 * Return an indexed iterator for the resultset represented by an
	 * associative array.
	 *
	 * @return iterator
	 */
	public function getIterator()
	{
		while (($row = $this->fetchRow()) !== [])
		{
			yield $row;
		}
	}

	/**
	 * Return an associative iterator for the resultset represented by an
	 * associative array.
	 *
	 * The specified key will be omitted from the row. If no key is supplied,
	 * then the first column is used.
	 *
	 * @param  string $key The column to use as the key
	 *
	 * @return iterator
	 */
	public function getKeyedIterator($key = '')
	{
		$key = (string) $key;
		$row = $this->fetchRow();

		$columnCount = count($row);

		if ($columnCount !== 0)
		{
			if ($key === '')
			{
				$key = key($row);
			}
			else if (array_key_exists($key, $row) === false)
			{
				throw new InvalidArgumentException("key $key does not exist");
			}

			if ($columnCount === 1)
			{
				do
				{
					yield $row[$key] => true;
				} while (($row = $this->fetchRow()) !== []);
			}
			else if ($columnCount === 2)
			{
				do
				{
					$keyValue = $row[$key];
					unset($row[$key]);
					yield $keyValue => current($row);
				} while (($row = $this->fetchRow()) !== []);
			}
			else
			{
				do
				{
					$keyValue = $row[$key];
					unset($row[$key]);
					yield $keyValue => $row;
				} while (($row = $this->fetchRow()) !== []);
			}
		}
	}

	/**
	 * Return an indexed iterator for the resultset represented by an indexed
	 * array.
	 *
	 * @return iterator
	 */
	public function getArrayIterator()
	{
		while (($row = $this->fetchRowAsArray()) !== [])
		{
			yield $row;
		}
	}

	/**
	 * Return an indexed iterator for a single column in the resultset.
	 *
	 * @param integer $index  The 0-based column index to return
	 *
	 * @return iterator
	 */
	public function getColumnIterator($index = 0)
	{
		while (($row = $this->fetchRowAsArray()) !== [])
		{
			yield $row[$index];
		}
	}

	/**
	 * Return an associative iterator for the resultset represented by an
	 * indexed array.
	 *
	 * The specified index will be omitted from the row. If no index is
	 * supplied, then the first column is used.
	 *
	 * @param  integer $index The column index to use as the key
	 *
	 * @return iterator
	 */
	public function getKeyedArrayIterator($index = 0)
	{
		$index = (int) $index;
		$row = $this->fetchRowAsArray();

		$columnCount = count($row);

		if ($index < 0 || $index >= $columnCount)
		{
			throw new InvalidArgumentException('index is out of range');
		}

		if ($columnCount === 1)
		{
			do
			{
				yield $row[0] => true;
			} while (($row = $this->fetchRowAsArray()) !== []);
		}
		else if ($columnCount === 2)
		{
			$valueIndex = 1 - $index;

			do
			{
				yield $row[$index] => $row[$valueIndex];
			} while (($row = $this->fetchRowAsArray()) !== []);
		}
		else
		{
			do
			{
				$keyValue = $row[$index];
				unset($row[$index]);
				yield $keyValue => array_values($row);
			} while (($row = $this->fetchRowAsArray()) !== []);
		}
	}

	/**
	 * Return an indexed iterator for the resultset, using the return value
	 * of the callback on each row.
	 *
	 * @param  callable $callback  A function to use to map the value
	 *
	 * @return iterator
	 */
	public function getCallbackIterator(callable $callback)
	{
		while (($row = $this->fetchRow()) !== [])
		{
			yield $callback($row);
		}
	}

	/**
	 * Return an associative iterator for the resultset, using the return value
	 * of the callback on each row.
	 *
	 * @param  callable $callback  A function to use to map the value
	 *                             It must return an array in the format of
	 *                             [string $key, array|object $row].
	 *
	 * @return iterator
	 */
	public function getKeyedCallbackIterator(callable $callback)
	{
		while (($row = $this->fetchRow()) !== [])
		{
			list($key, $value) = $callback($row);
			yield $key => $value;
		}
	}
}