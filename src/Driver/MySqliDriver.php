<?php namespace mef\Db\Driver;

use mysqli;

use mef\Db\RecordSet\MySqliRecordSet;
use mef\Db\Statement\MySqliStatement;
use mef\Db\Driver\Exception\QueryException;
use mef\Db\Driver\Exception\PrepareException;
use mef\Db\Driver\Exception\ExecuteException;

class MySqliDriver extends AbstractDriver
{
	/**
	 * @var mysqli
	 */
	protected $mysqli;

	/**
	 * Constructor
	 *
	 * @param mysql $mysqli
	 */
	public function __construct(mysqli $mysqli)
	{
		$this->mysqli = $mysqli;
	}

	/**
	 * Return the underlying PHP mysqli object.
	 *
	 * @return mysqli
	 */
	final public function getMySqli()
	{
		return $this->mysqli;
	}

	/**
	 * Returns a buffered RecordSet for the given query.
	 *
	 * This must only be used with queries that return a record set.
	 * For DELETE, INSERT, UPDATE, etc, use Driver::exec().
	 *
	 * @param string $sql  An SQL SELECT statement
	 *
	 * @return \mef\DB\RecordSet\MySqlRecordset
	 */
	public function query($sql)
	{
		if ($this->mysqli->real_query($sql) === false)
		{
			$result = false;
		}
		else
		{
			$result = $this->mysqli->store_result();
		}

		if ($result === false)
		{
			throw new QueryException($sql, $this->mysqli->error);
		}

		return new MySqliRecordset($result);
	}

	/**
	 * Executes the given query.
	 *
	 * @param string $sql  An SQL non-SELECT statement (UPDATE, DELETE, etc)
	 *
	 * @return int         The number of rows affected (if applicable / supported)
	 */
	public function execute($sql)
	{
		if ($this->mysqli->real_query($sql) === false ||
			$this->mysqli->store_result() === false)
		{
			throw new ExecuteException($sql, $this->mysqli->error);
		}

		return $this->mysqli->affected_rows;
	}

	/**
	 * Prepares the SQL statement, optionally binds the list of
	 * arguments, and returns a Statement.
	 *
	 * <code>
	 * $db->prepare('SELECT * FROM foo WHERE bar=?', [42]);
	 * </code>
	 *
	 * @param string $sql   An SQL statement
	 * @param array $params The parameters to bind (optional)
	 *
	 * @return \mef\DB\Statement
	 */
	public function prepare($sql, array $params = [])
	{
		$parameters = [];

		/**
		 * MySqli doesn't support named parameters. Emulate this feature.
		 *
		 * @todo This needs to be parsed much more carefully.
		 */
		if (preg_match_all('/:[A-za-z0-9_]+/', $sql, $matches, PREG_OFFSET_CAPTURE | PREG_SET_ORDER))
		{
			$j = count($matches);
			foreach (array_reverse($matches) as $i => $match)
			{
				list($parameter, $offset) = $match[0];

				$sql = substr($sql, 0, $offset) . '?' . substr($sql, $offset + strlen($parameter));
				$parameters[$parameter][] = --$j;
			}
		}

		$mysqliStmt = $this->mysqli->prepare($sql);

		if ($mysqliStmt === false)
		{
			throw new PrepareException($sql, $this->mysqli->error);
		}

		$st = new MySqliStatement($mysqliStmt, $parameters);
		$st->setParameters($params);
		return $st;
	}

	/**
	 * Return a value quoted safely for the current connection.
	 *
	 * @param  string $value
	 *
	 * @return string
	 */
	public function quoteValue($value)
	{
		return $this->mysqli->real_escape_string($value);
	}
}
