<?php namespace mef\Db\Driver;

use Exception;
use InvalidArgumentException;
use PDO;
use PDOException;

use mef\Db\RecordSet\PdoRecordSet;
use mef\Db\Statement\PdoStatement;

use mef\Db\Driver\Exception\QueryException;
use mef\Db\Driver\Exception\PrepareException;
use mef\Db\Driver\Exception\ExecuteException;

class PdoDriver extends AbstractDriver
{
	const ANSI_ERROR_CODE_INDEX = 0;
	const DRIVER_ERROR_CODE_INDEX = 1;
	const DRIVER_ERROR_MESSAGE_INDEX = 2;

	protected $pdo;

	public function __construct(PDO $pdo)
	{
		$this->pdo = $pdo;
	}

	/**
	 * Return the underlying PHP PDO object.
	 *
	 * @return PDO  The underlying PHP PDO object.
	 */
	final public function getPDO()
	{
		return $this->pdo;
	}

	/**
	 * {@inheritdoc}
	 *
	 * @return \mef\Db\RecordSet\PdoRecordSet
	 */
	public function query($sql)
	{
		try
		{
			$pdoQuery = $this->pdo->query($sql);
		}
		catch (PDOException $e)
		{
			$pdoQuery = false;
		}

		if ($pdoQuery === false)
		{
			throw new QueryException($sql, $this->getErrorInfoString());
		}

		return new PdoRecordSet($pdoQuery);
	}

	/**
	 * {@inheritdoc}
	 */
	public function execute($sql)
	{
		try
		{
			$affectedRows = $this->pdo->exec($sql);
		}
		catch (PDOException $e)
		{
			$affectedRows = false;
		}

		if ($affectedRows === false)
		{
			throw new ExecuteException($sql, $this->getErrorInfoString());
		}

		return $affectedRows;
	}

	/**
	 * {@inheritdoc}
	 *
	 * @return \mef\Db\Statement\PdoStatement
	 */
	public function prepare($sql, array $params = [])
	{
		try
		{
			$pdoStatement = $this->pdo->prepare($sql);
		}
		catch (PDOException $e)
		{
			$pdoStatement = false;
		}

		if ($pdoStatement === false)
		{
			throw new PrepareException($sql, $this->getErrorInfoString());
		}

		$st = new PdoStatement($pdoStatement);
		$st->setParameters($params);

		return $st;
	}

	/**
	 * Safely quote the value for the current connection.
	 *
	 * @param  string $value
	 *
	 * @return string
	 */
	public function quoteValue($value)
	{
		return $this->pdo->quote($value);
	}

	/**
	 * Return an error message as a string from the last PDO error.
	 *
	 * @return string
	 */
	private function getErrorInfoString()
	{
		$errorInfo = $this->pdo->errorInfo();
		return '[' . $errorInfo[self::ANSI_ERROR_CODE_INDEX] . '] ' . $errorInfo[self::DRIVER_ERROR_MESSAGE_INDEX];
	}
}
