<?php namespace mef\Db\Driver;

/**
 * Decorate a database driver with additional functionality.
 */
abstract class AbstractDecoratorDriver implements DriverInterface
{
	/**
	 * @var \ef\Db\Driver\DriverInterface
	 */
	protected $db;

	/**
	 * Constructor
	 *
	 * @param \mef\Db\Driver\DriverInterface $db The database object that is
	 *                                           being "extended."
	 */
	public function __construct(DriverInterface $db)
	{
		$this->db = $db;
	}

	/**
	 * {@inheritdoc}
	 */
	public function startTransaction()
	{
		return $this->db->startTransaction();
	}

	/**
	 * {@inheritdoc}
	 */
	public function commit()
	{
		return $this->db->commit();
	}

	/**
	 * {@inheritdoc}
	 */
	public function rollBack()
	{
		return $this->db->rollBack();
	}

	/**
	 * {@inheritdoc}
	 */
	public function prepare($sql, array $params = [])
	{
		return $this->db->prepare($sql, $params);
	}

	/**
	 * {@inheritdoc}
	 */
	public function query($sql)
	{
		return $this->db->query($sql);
	}

	/**
	 * {@inheritdoc}
	 */
	public function execute($sql)
	{
		return $this->db->execute($sql);
	}

	/**
	 * {@inheritdoc}
	 */
	public function quoteValue($value)
	{
		return $this->db->quoteValue($value);
	}
}