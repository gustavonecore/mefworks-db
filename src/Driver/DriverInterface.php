<?php namespace mef\Db\Driver;

interface DriverInterface
{
	/**
	 * Begin a transaction.
	 */
	public function startTransaction();

	/**
	 * Commit the existing transaction.
	 */
	public function commit();

	/**
	 * Roll back the current transaction.
	 */
	public function rollBack();

	/**
	 * Prepares the SQL statement, optionally binds the list of
	 * arguments, and returns a Statement.
	 *
	 * <code>
	 * $db->prepare('SELECT * FROM foo WHERE bar=?', [42]);
	 * </code>
	 *
	 * @param string $sql   An SQL statement
	 * @param array $params The parameters to bind (optional)
	 *
	 * @return \mef\Db\Statement\StatementInterface
	 */
	public function prepare($sql, array $params = []);

	/**
	 * Return a buffered RecordSet for the given query.
	 *
	 * This must only be used with queries that return a record set.
	 * For DELETE, INSERT, UPDATE, etc, use DriverInterface::execute().
	 *
	 * @param string $sql  An SQL SELECT statement
	 *
	 * @return \mef\Db\RecordSet\RecordSetInterface
	 */
	public function query($sql);

	/**
	 * Execute the given query.
	 *
	 * @param string $sql  An SQL non-SELECT statement (UPDATE, DELETE, etc)
	 *
	 * @return integer     The number of rows affected (if applicable / supported)
	 */
	public function execute($sql);

	/**
	 * Safely quote the value for the current connection.
	 *
	 * @param  string $value
	 *
	 * @return string
	 */
	public function quoteValue($value);
}