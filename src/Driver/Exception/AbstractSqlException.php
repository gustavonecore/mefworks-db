<?php namespace mef\Db\Driver\Exception;

use Exception;

abstract class AbstractSqlException extends Exception
{
	/**
	 * @var string
	 */
	protected $sql;

	/**
	 * @var string
	 */
	protected $driverMessage;

	/**
	 * Constructor
	 *
	 * @param string $message       a message describing the error
	 * @param string $sql           the SQL that triggered the error
	 * @param string $driverMessage the message from the driver
	 */
	public function __construct($message, $sql, $driverMessage = '')
	{
		$this->sql = (string) $sql;
		$this->driverMessage = (string) $driverMessage;

		parent::__construct($message . $this->sql . rtrim("\n" . $this->driverMessage));
	}

	/**
	 * Return the SQL that triggered the error.
	 *
	 * @return string
	 */
	public function getSql()
	{
		return $this->sql;
	}

	/**
	 * Return the error message from the driver.
	 *
	 * @return string
	 */
	public function getDriverMessage()
	{
		return $this->driverMessage;
	}
}