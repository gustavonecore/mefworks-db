<?php namespace mef\Db\Driver\Exception;

class QueryException extends AbstractSqlException
{
	/**
	 * Constructor
	 *
	 * @param string $sql           the SQL that triggered the error
	 * @param string $driverMessage the message from the driver
	 */
	public function __construct($sql, $driverMessage = '')
	{
		parent::__construct('Unable to query SQL: ', $sql, $driverMessage);
	}
}