<?php namespace mef\Db\Driver;

use mef\Db\Driver\DataProvider\DataProviderInterface;
use mef\Db\RecordSet\IteratorRecordSet;
use mef\Db\Statement\DataProviderStatement;

class DataProviderDriver extends AbstractDriver
{
	/**
	 * @var \mef\Db\Driver\DataProvider\DataProviderInterface
	 */
	private $dataProvider;

	public function __construct(DataProviderInterface $dataProvider)
	{
		$this->dataProvider = $dataProvider;
	}

	/**
	 * Return the data provider
	 *
	 * @return \mef\DB\Mock\DataProviderInterface
	 */
	public function getDataProvider()
	{
		return $this->dataProvider;
	}

	/**
	 * Returns an unbuffered RecordSet for the given query.
	 *
	 * This must only be used with queries that return a record set.
	 * For DELETE, INSERT, UPDATE, etc, use Driver::exec().
	 *
	 * @param string $sql  An SQL SELECT statement
	 *
	 * @return \mef\Db\RecordSet\IteratorRecordSet
	 */
	public function query($sql)
	{
		return new IteratorRecordSet($this->dataProvider->getDataForQuery($sql));
	}

	/**
	 * Executes the given query.
	 *
	 * @param string $sql  An SQL non-SELECT statement (UPDATE, DELETE, etc)
	 *
	 * @return int         The number of rows affected (if applicable / supported)
	 */
	public function execute($sql)
	{
		return $this->dataProvider->executeQuery($sql);
	}

	/**
	 * Prepares the SQL statement, optionally binds the list of
	 * arguments, and returns a Statement.
	 *
	 * <code>
	 * $db->prepare('SELECT * FROM foo WHERE bar=?', [42]);
	 * </code>
	 *
	 * @param string $sql       An SQL statement
	 * @param array $parameters The parameters to bind (optional)
	 *
	 * @return \mef\Db\Statement\DataProviderStatement
	 */
	public function prepare($sql, array $parameters = [])
	{
		return new DataProviderStatement($this->dataProvider, $sql, $parameters);
	}

	/**
	 * Quote the string so that it is protected against SQL injection.
	 *
	 * Note that this function does nothing useful! This is a mock driver
	 * and it doesn't really try to provide any security.
	 *
	 * @param  string $value  The value to quote
	 *
	 * @return  string
	 */
	public function quoteValue($value)
	{
		return '"' . addslashes((string) $value) . '"';
	}
}